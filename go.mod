module gitlab.com/ed13710/vidstrcol

require (
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	github.com/segmentio/kafka-go v0.2.2
	github.com/stretchr/testify v1.2.2
	gocv.io/x/gocv v0.18.0
)
