package main

import (
	"fmt"
	"image"
	"log"
	"strconv"
	"context"
	"time"

	"github.com/segmentio/kafka-go"
	"gocv.io/x/gocv"

	
	"gitlab.com/ed13710/vidstrcol/entity"
)

var (
	brokers             = []string{"192.168.10.30:9092"}
	topic  				= string("video-stream")
)

func main() {

	w := kafka.NewWriter(kafka.WriterConfig{
		Brokers: brokers,
		Topic:   topic,
		Balancer: &kafka.LeastBytes{},
	})


	// parse args
	deviceID := 0

	// open webcam
	webcam, err := gocv.VideoCaptureDevice(int(deviceID))
	if err != nil {
		fmt.Println(err)
		return
	}
	defer webcam.Close()

	// prepare image matrix
	img := gocv.NewMat()
	defer img.Close()

	fmt.Printf("start reading camera device: %v\n", deviceID)
	cpt := 0
	for {
		if ok := webcam.Read(&img); !ok {
			fmt.Printf("cannot read device %d\n", deviceID)
			return
		}
		if img.Empty() {
			continue
		}
		//fmt.Printf("read one image of length %d\n", len(img.ToBytes()))

		gocv.Resize(img, &img, image.Point{X: 640, Y: 480}, 0, 0, gocv.InterpolationCubic)

		cols := img.Cols()
		rows := img.Rows()
		typeStr := int(img.Type())

		data2 := img.ToBytes()

		message := entity.EncodedMessage{
			CameraID:  "cameraId",
			Cols:      cols,
			Rows:      rows,
			Type:      typeStr,
			Timestamp: time.Now(),
			Data:      data2,
		}

		dataToEncode, err := message.Encode(message)
		if (err != nil) {
			log.Fatalf("error encoding message: %v", err)
		} else {
			err = w.WriteMessages(context.Background(),
				kafka.Message{
					Key:   []byte(strconv.Itoa(cpt)),
					Value: dataToEncode,
				},
			)
			if (err != nil) {
			log.Fatalf("error writing message: %v", err)
		}  else {
			fmt.Printf("sent message %d on topic\n", cpt)
		}
		}
	
		//err = emitter.EmitSync(strconv.Itoa(cpt), message)
		if err != nil {
			log.Fatalf("error emitting message: %v", err)
		}

		

		cpt = cpt + 1
	}
}
