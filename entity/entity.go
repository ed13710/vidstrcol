package entity

import (
	"encoding/json"
	"fmt"
	"strconv"
	"strings"
	"time"

	b64 "encoding/base64"
)

/*
EncodedMessage contains information about the  MAT sent as data
*/
type EncodedMessage struct {
	CameraID  string
	Timestamp time.Time
	Rows      int
	Cols      int
	Type      int
	Data      []byte
}

/*
Decode from a byte array to an EncodedMessage
*/
func (*EncodedMessage) Decode(data []byte) (interface{}, error) {
	var rawStrings map[string]string

	var l EncodedMessage
	err := json.Unmarshal(data, &rawStrings)
	if err != nil {
		return nil, err
	}

	fmt.Println("decoding")
	for k, v := range rawStrings {

		if strings.ToLower(k) == "cameraId" {
			l.CameraID = v
		}
		if strings.ToLower(k) == "timestamp" {
			t, err := time.Parse(time.RFC3339, v)
			if err != nil {
				return nil, err
			}
			l.Timestamp = t
		}
		if strings.ToLower(k) == "rows" {
			l.Rows, err = strconv.Atoi(v)
			if err != nil {
				return nil, err
			}
		}
		if strings.ToLower(k) == "cols" {
			l.Cols, err = strconv.Atoi(v)
			if err != nil {
				return nil, err
			}
		}
		if strings.ToLower(k) == "type" {
			l.Type, err = strconv.Atoi(v)
			if err != nil {
				return nil, err
			}
		}

		if strings.ToLower(k) == "data" {
			fmt.Println("data" + v)
			l.Data, err = b64.StdEncoding.DecodeString(v)
			if err != nil {
				return nil, err
			}
		}
	}

	return l, nil
}

/*
Encode EncodedMessage into a byte array to an
*/
func (*EncodedMessage) Encode(value interface{}) ([]byte, error) {

	var err error
	l, isEncodedMessage := value.(EncodedMessage)
	if !isEncodedMessage {
		err = fmt.Errorf("DefaultCodec: value to encode is not of type isEncodedMessage")
		return nil, err
	}

	basicEncodedMessage := struct {
		CameraID  string `json:"cameraId"`
		Timestamp string `json:"timestamp"`
		Rows      int    `json:"rows,string"`
		Cols      int    `json:"cols,string"`
		Type      int    `json:"type,string"`
		Data      string `json:"data"`
	}{
		CameraID:  l.CameraID,
		Timestamp: l.Timestamp.Format(time.RFC3339),
		Rows:      l.Rows,
		Cols:      l.Cols,
		Type:      l.Type,
		Data:      b64.StdEncoding.EncodeToString(l.Data),
	}

	return json.Marshal(basicEncodedMessage)
}
