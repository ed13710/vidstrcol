package entity

import (
	b64 "encoding/base64"
	"fmt"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"gitlab.com/ed13710/videostreamcollector/entity"
)

func TestBase64(t *testing.T) {

	data := "testData"
	encoded := b64.StdEncoding.EncodeToString([]byte(data))
	assert.NotNil(t, encoded)
	decoded, err := b64.StdEncoding.DecodeString(encoded)

	assert.Nil(t, err)
	assert.NotNil(t, decoded)

}
func TestCodec(t *testing.T) {
	entity := entity.EncodedMessage{
		CameraID:  "theCameraId",
		Timestamp: time.Now(),
		Rows:      3,
		Cols:      4,
		Type:      5,
		Data:      []byte("testData"),
	}

	encoded, err := entity.Encode(entity)

	fmt.Println(string(encoded))
	assert.Nil(t, err)
	assert.NotNil(t, encoded)

	decoded, err := entity.Decode(encoded)
	assert.Nil(t, err)
	fmt.Println(err)
	assert.NotNil(t, decoded)

}
